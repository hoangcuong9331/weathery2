//
//  LocationHelper.swift
//  Weathery
//
//  Created by Macbook on 2/16/19.
//  Copyright © 2019 Spiritofthecore. All rights reserved.
//

import UIKit

class LocationHelper {
    
    let AccuWeatherAPIKey = "bVuiS0qMrVYNEw3nTacGX5OaKkNyQXlB"
    func locationRequest(searchText: String, completion: @escaping ([Location]) -> ()) {
        var CityList: [Location] = []
        let urlString = "http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=\(AccuWeatherAPIKey)&q=\(searchText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlString!)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            do {
                if let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSArray {
                    for city in dic {
                        let CityName = (city as! NSDictionary).value(forKey: "LocalizedName") as! String
                        let CityKey = (city as! NSDictionary).value(forKey: "Key") as! String
                        
                        CityList.append(Location(cityName: CityName, cityKey: CityKey))
                        
                        print(city)
                    }
                    completion(CityList)
                } else {
                    completion(CityList)
                    return
                }
            } catch {
                print("error")
            }
        } ).resume()
    }
    
    func GetTime(at location: Location) {
        let urlString = "http://dataservice.accuweather.com/locations/v1/\(location.CityKey)?apikey=\(AccuWeatherAPIKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlString!)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            do {
                if let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary {
                    print(dic)
                    if let GMT = (dic.value(forKey: "TimeZone") as! NSDictionary).value(forKey: "GmtOffset") as? Int {
                        DispatchQueue.main.async {
                            self.timeList.append((location, GMT))
                            self.tableView.reloadData()
                        }
                    }
                } else {
                    return
                }
            } catch {
                print("error")
                return
            }
        } ).resume()
    }
}
