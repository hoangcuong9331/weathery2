//
//  TimeViewController.swift
//  Weathery
//
//  Created by Macbook on 2/10/19.
//  Copyright © 2019 Spiritofthecore. All rights reserved.
//

import UIKit

class TimeViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, TimeHandler{
    func DeleteLocation(at indexPath: IndexPath) {
        TimeViewController.timeList.remove(at: indexPath.row)
        if searchResultController.sharedInstance.weatherHandler != nil && ViewController.LocationList.count > indexPath.row {
            searchResultController.sharedInstance.weatherHandler.DeleteLocation(at: indexPath)
        }
        tableView.reloadData()
    }
    
    
    static var timeList: [(Location, Int)] = []
    var timer = Timer()
    var isEditting: Bool! = false
    
    func locationRequest(searchText: String, completion: @escaping ([Location]) -> ()) {
        var CityList: [Location] = []
        let urlString = "http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=\(AccuWeatherAPIKey)&q=\(searchText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlString!)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            do {
                if let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSArray {
                    for city in dic {
                        let CityName = (city as! NSDictionary).value(forKey: "LocalizedName") as! String
                        let CityKey = (city as! NSDictionary).value(forKey: "Key") as! String
                        
                        CityList.append(Location(cityName: CityName, cityKey: CityKey))
                        
                        print(city)
                    }
                    completion(CityList)
                } else {
                    changeAPI(current: AccuWeatherAPIKey)
                    completion(CityList)
                    return
                }
            } catch {
                print("error")
            }
        } ).resume()
    }
    
    func GetTime(at location: Location) {
        let urlString = "http://dataservice.accuweather.com/locations/v1/\(location.CityKey)?apikey=\(AccuWeatherAPIKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlString!)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            do {
                if let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary {
                    print(dic)
                    if let GMT = (dic.value(forKey: "TimeZone") as! NSDictionary).value(forKey: "GmtOffset") as? Int {
                        DispatchQueue.main.async {
                            TimeViewController.timeList.append((location, GMT))
                            self.tableView.reloadData()
                        }
                    }
                } else {
                    changeAPI(current: AccuWeatherAPIKey)
                    return
                }
            } catch {
                print("error")
                return
            }
        } ).resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if TimeViewController.timeList.count == 0 {
            tableView.setEmptyView(title: "You don't choose any location.", message: "Your world time clock will be in here.")
        }
        else {
            tableView.restore()
        }
        return TimeViewController.timeList.count
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = TimeViewController.timeList[sourceIndexPath.row]
        TimeViewController.timeList.remove(at: sourceIndexPath.row)
        TimeViewController.timeList.insert(movedObject, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            DeleteLocation(at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeCell", for: indexPath)
        let secondsFromGMT = Double((3600*TimeViewController.timeList[indexPath.row].1 - TimeZone.current.secondsFromGMT()))
        let GMTNumber = TimeViewController.timeList[indexPath.row].1
        let GMTString = "GMT " + ((GMTNumber > 0) ? "+\(GMTNumber)": "\(GMTNumber)")

        let date = NSDate.init(timeIntervalSinceNow: secondsFromGMT)
        
        let dateString = DateFormatter.localizedString(from: date as Date, dateStyle: .medium, timeStyle: .medium)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = """
        \(TimeViewController.timeList[indexPath.row].0.CityName)
        \(dateString) \(GMTString)
        """
        cell.backgroundColor = UIColor.clear
        return cell
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        locationRequest(searchText: searchText) { (CityList) in
            DispatchQueue.main.async {
//                self.searchresultController.reloadDataWithArray(CityList)
                searchResultController.sharedInstance.reloadDataWithArray(CityList)
            }
        }
    }
    
    
    


    @IBOutlet weak var tableView: UITableView!
    
    
    @IBAction func searchLocation(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: searchResultController.sharedInstance)
        searchController.searchBar.delegate = self
        self.present(searchController, animated: true, completion: nil)
    }
    
    @IBAction func EditTable(_ sender: Any) {
        if isEditing == true {
            timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (_) in
                self.tableView.reloadData()
            }
            self.navigationItem.leftBarButtonItem! = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(self.EditTable(_:)))
            self.tableView.isEditing = false
            isEditing = false
        } else {
            timer.invalidate()
            self.navigationItem.leftBarButtonItem! = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.EditTable(_:)))
            self.tableView.isEditing = true
            isEditing = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        searchresultController = searchResultController()
//        searchresultController.timeHandler = self
        searchResultController.sharedInstance.timeHandler = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "TimeCell")
        tableView.dataSource = self
        tableView.delegate = self
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (_) in
            self.tableView.reloadData()
        }
        if TimeViewController.timeList.count < ViewController.LocationList.count {
            for i in TimeViewController.timeList.count...ViewController.LocationList.count - 1 {
                GetTime(at: ViewController.LocationList[i])
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        searchResultController.sharedInstance.viewKey = 2

    }
}

extension UITableView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.backgroundColor = UIColor(red: 255/256, green: 255/256, blue: 255/256, alpha: 0.7)
        emptyView.layer.cornerRadius = 5
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        //titleLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UICollectionView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.backgroundColor = UIColor(red: 255/256, green: 255/256, blue: 255/256, alpha: 0.7)
        emptyView.layer.cornerRadius = 5
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        //titleLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
    }
    func restore() {
        self.backgroundView = nil
    }
}
