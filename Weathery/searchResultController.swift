//
//  searchResultController.swift
//  Weathery
//
//  Created by Macbook on 1/24/19.
//  Copyright © 2019 Spiritofthecore. All rights reserved.
//

import UIKit

//protocol SearchResultsHandler {
//    func GetWeather(at location: Location)
//    func GetTime(at location: Location)
//    func GetCoordinate(at location: Location)
//}

protocol WeatherHandler {
    func GetWeather(at location: Location)
    func DeleteLocation(at indexPath: IndexPath)
}
protocol TimeHandler {
    func GetTime(at location: Location)
    func DeleteLocation(at indexPath: IndexPath)

}
protocol CoordinateHandler {
    func GetCoordinate(at location: Location)
}

class searchResultController: UITableViewController {
    
//    var delegate: SearchResultsHandler!
    var weatherHandler: WeatherHandler!
    var timeHandler: TimeHandler!
    var coordinateHandler: CoordinateHandler!
    var searchResults: [Location]!
    var viewKey: Int!
    static var sharedInstance: searchResultController = searchResultController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchResults = Array()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "LocationCell")
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.searchResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath)

        cell.textLabel?.text = self.searchResults[indexPath.row].CityName
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
//        self.delegate.GetWeather(at: searchResults[indexPath.row])
//        self.delegate.GetTime(at: searchResults[indexPath.row])
//        self.delegate.GetCoordinate(at: searchResults[indexPath.row])
        if viewKey == 3 {
            self.coordinateHandler.GetCoordinate(at: searchResults[indexPath.row])
        } else {
        if timeHandler != nil {
            self.timeHandler.GetTime(at: searchResults[indexPath.row])
        }
        if weatherHandler != nil {
            self.weatherHandler.GetWeather(at: searchResults[indexPath.row])
        }
        }
    }
    
    func reloadDataWithArray(_ array: [Location]) {
        self.searchResults = array
        self.tableView.reloadData()
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
