//
//  GoogleMapViewController.swift
//  Weathery
//
//  Created by Macbook on 2/9/19.
//  Copyright © 2019 Spiritofthecore. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
//import GooglePlaces

class GoogleMapViewController: UIViewController, UISearchBarDelegate, CoordinateHandler, MKMapViewDelegate, CLLocationManagerDelegate{
    
    let locationManager = CLLocationManager()
    let userAnnotation = MKPointAnnotation()

    @IBOutlet weak var mapView: MKMapView!
    var source: (CLLocationCoordinate2D, String) = (CLLocationCoordinate2D.init(), "")
    var destination: (CLLocationCoordinate2D, String) = (CLLocationCoordinate2D.init(), "")
    var searchForDestination: Bool!
    
    @IBAction func Cancel(_ sender: Any) {
        source = (CLLocationCoordinate2D.init(), "")
        destination = (CLLocationCoordinate2D.init(), "")
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.addAnnotation(userAnnotation)
        self.mapView.removeOverlays(self.mapView.overlays)
    }
    @IBAction func SeachSourceLocation(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: searchResultController.sharedInstance)
        searchController.searchBar.delegate = self
        searchForDestination = false
        self.present(searchController, animated: true, completion: nil)
    }
    @IBAction func SearchAddress(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: searchResultController.sharedInstance)
        searchController.searchBar.delegate = self
        searchForDestination = true
        self.present(searchController, animated: true, completion: nil)
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Maps"
        mapView.delegate = self
        //navigationController?.title = "Maps"
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
             guard let locValue: CLLocationCoordinate2D = locationManager.location?.coordinate else { return }
            userAnnotation.coordinate = locValue
            userAnnotation.title = "You"
            self.mapView.addAnnotation(userAnnotation)
            let region = MKCoordinateRegion(center: userAnnotation.coordinate, latitudinalMeters: CLLocationDistance(exactly: 50000)!, longitudinalMeters: CLLocationDistance(exactly: 50000)!)
            self.mapView.setRegion(self.mapView.regionThatFits(region), animated: true)
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        searchResultController.sharedInstance.coordinateHandler = self
        searchResultController.sharedInstance.viewKey = 3
//        searchresultController.viewKey = 3
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        userAnnotation.coordinate = locValue
    }
    
    func locationsIsSet() -> Bool{
        if self.source.1 != "" && self.destination.1 != "" {
            return true
        }
        return false
    }
    
    func locateWithLongitude(lon: Double, andLatitude lat: Double, andTitle title: String) {
        DispatchQueue.main.async { () -> Void in
            
//            let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
//            let marker = GMSMarker(position: position)
//            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
//            
//            self.GoogleMapsView.camera = camera
//            
//            marker.title = "\(title)"
//            self.navigationItem.title = marker.title
//            marker.map = self.GoogleMapsView
            if self.locationsIsSet() == false {
                let annotation = MKPointAnnotation()
                if self.searchForDestination == false {
                    self.source.0 = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    self.source.1 = title
                    annotation.coordinate = self.source.0
                    annotation.title = self.source.1
                } else {
                    self.destination.0 = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    self.destination.1 = title
                    annotation.coordinate = self.destination.0
                    annotation.title = self.destination.1
                }
                self.mapView.addAnnotation(annotation)
                let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: CLLocationDistance(exactly: 50000)!, longitudinalMeters: CLLocationDistance(exactly: 50000)!)
                self.mapView.setRegion(self.mapView.regionThatFits(region), animated: true)
                //self.mapView.setCenter(self.source.0, animated: true)
            }
            if self.locationsIsSet() == true {
                if self.searchForDestination == false {
                    self.source.0 = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    self.source.1 = title
                } else {
                    self.destination.0 = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    self.destination.1 = title
                }
                let sourcePlacemark = MKPlacemark(coordinate: self.source.0, addressDictionary: nil)
                let destinationPlacemark = MKPlacemark(coordinate: self.destination.0, addressDictionary: nil)
                
                let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
                let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
                let sourceAnnotation = MKPointAnnotation()
                sourceAnnotation.title = self.source.1
                if let location = sourcePlacemark.location {
                    sourceAnnotation.coordinate = location.coordinate
                }
                let destinationAnnotation = MKPointAnnotation()
                destinationAnnotation.title = self.destination.1
                
                if let location = destinationPlacemark.location {
                    destinationAnnotation.coordinate = location.coordinate
                }
            self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
                
                let directionRequest = MKDirections.Request()
                directionRequest.source = sourceMapItem
                directionRequest.destination = destinationMapItem
                directionRequest.transportType = .automobile
                
                let directions = MKDirections(request: directionRequest)
                
                directions.calculate {
                    (response, error) -> Void in
                    
                    guard let response = response else {
                        if let error = error {
                            print("Error: \(error)")
                        }
                        
                        return
                    }
                    let route = response.routes[0]
                    self.mapView.addOverlay((route.polyline), level: .aboveRoads)
                    
                    let rect = route.polyline.boundingMapRect
                    self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
                    self.mapView.setVisibleMapRect(rect, animated: true)
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        return MKPolylineRenderer.init()
    }
    
    func GetCoordinate(at location: Location) {
        let urlString = "http://dataservice.accuweather.com/locations/v1/\(location.CityKey)?apikey=\(AccuWeatherAPIKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlString!)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            do {
                if let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary {
                    print(dic)
                    guard let lat = (dic.value(forKey: "GeoPosition") as! NSDictionary).value(forKey: "Latitude") as? Double else {
                        return
                    }
                    guard let lon = (dic.value(forKey: "GeoPosition") as! NSDictionary).value(forKey: "Longitude") as? Double else {
                        return
                    }
                    self.locateWithLongitude(lon: lon, andLatitude: lat, andTitle: location.CityName)
                } else {
                    //changeAPI(current: AccuWeatherAPIKey)
                    return
                }
            } catch {
                print("error")
                return
            }
        } ).resume()
    }
    
    func locationRequest(searchText: String, completion: @escaping ([Location]) -> ()) {
        var CityList: [Location] = []
        let urlString = "http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=\(AccuWeatherAPIKey)&q=\(searchText)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlString!)
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            do {
                if let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSArray {
                    for city in dic {
                        let CityName = (city as! NSDictionary).value(forKey: "LocalizedName") as! String
                        let CityKey = (city as! NSDictionary).value(forKey: "Key") as! String
                        
                        CityList.append(Location(cityName: CityName, cityKey: CityKey))
                        
                        print(city)
                    }
                    completion(CityList)
                } else {
                    changeAPI(current: AccuWeatherAPIKey)
                    completion(CityList)
                    return
                }
            } catch {
                print("error")
            }
        } ).resume()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        locationRequest(searchText: searchText) { (CityList) in
            DispatchQueue.main.async {
//                self.searchresultController.reloadDataWithArray(CityList)
                searchResultController.sharedInstance.reloadDataWithArray(CityList)
            }
        }
    }
}

